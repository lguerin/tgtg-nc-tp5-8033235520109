import { Component, Input, OnInit } from '@angular/core';
import { StoreModel } from '../../../models/store.model';
import { TgtgService } from '../../../services/tgtg.service';

@Component({
  selector: 'nc-store-card',
  templateUrl: './store-card.component.html',
  styleUrls: ['./store-card.component.scss']
})
export class StoreCardComponent implements OnInit {

  @Input()
  store: StoreModel;

  isFavorite = false;

  constructor(private tgtgService: TgtgService) { }

  ngOnInit(): void {
    this.isFavorite = this.tgtgService.isFavorite(this.store);
  }

  toggle(): void {
    this.isFavorite = this.tgtgService.toggleFavorite(this.store);
  }
}
